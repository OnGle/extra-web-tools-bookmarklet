let ewt={};

ewt.el = function(name, root, params) {
	let el = document.createElement(name);
	if (root != null) {
		root.appendChild(el);
	}
	if (params != null) {
		Object.assign(el, params);
	}
	return el;
}

let root = ewt.el("div", document.body);
root.id = "ewt-root-node";
let style = ewt.el("style", root).innerText = `
#ewt-root-node {
	all: unset;
	position: fixed;
	margin: 10px;
	padding: 10px;
	background: black;
	right: 0px;
	top: 0px;
	font-family: monospace;
	font-size: 14px;
	z-index: 9999;
	border: 1px solid white;
	width: 25%;
}
#ewt-root-node * {
	all: unset;
	font-family: monospace;
	font-size: 14px;
}
#ewt-root-node style {
	display: none;
}
#ewt-root-node tr {
	display: table-row;
}
#ewt-root-node td {
	display: table-cell;
	box-sizing: border-box;
}
#ewt-root-node td > *{
	display: inline-block;
	box-sizing: border-box;
	height: 100%;
	width: 100%;
}
#ewt-root-node table {
	display: table;
	list-style-type: none;
	table-layout: fixed;
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 1px;
	border-spacing: 2;
}
#ewt-root-node input {
	margin: 2px;
	padding: 2px 4px 2px 4px;
	background: black;
	color: white;
	border: 1px solid white;
}
#ewt-root-node input[type=button] {
	background: #272747;
}
#ewt-root-node textarea {
	white-space: break-spaces;
	height: 100%;
}
#ewt-root-node textarea:hover {
	background: #373777;
}
#ewt-root-node textarea {
	margin: 2px;
	padding: 2px 4px 2px 4px;
	background: black;
	color: white;
	border: 1px solid white;
}
#ewt-root-node input:hover {
	background: #373777;
}
#ewt-root-node input:active {
	background: white;
	color: black;
}

`;
let list = ewt.el("table", root);

ewt.row = function() {
	return ewt.el("tr", list);
}

let close_row = ewt.row();
ewt.el("td", close_row);
ewt.el("input", ewt.el("td", close_row), {
	onclick: function() {
		document.body.removeChild(root);
		ewt = null;
	},
	type: "button",
	value: "Close"
});

let r1 = ewt.row();

ewt.el("input", ewt.el("td", r1), {
	onclick: function() {
		document.body.style.overflow = "visible";
	},
	type: "button",
	value: "Unlock Scroll"
});

ewt.el("input", ewt.el("td", r1), {
	onclick: function() {
		let tag_types = ["input", "button", "textarea", "select"];
		for (const tag of tag_types) {
			for (const r of document.getElementsByTagName(tag)) {
				r.disabled = false;
			}
		}
	},
	type: "button",
	value: "Enable Disabled Inputs"
});

ewt.el("br", ewt.row());

const xpath_row = ewt.row();
const xpath_input = ewt.el("input", ewt.el("td", xpath_row), {
	placeholder: "//some/xpath/query",
	type: "input"
});
ewt.el("input", ewt.el("td", xpath_row), {
	onclick: function() {
		console.log("XPath Query Results:");
		const query = document.evaluate(xpath_input.value, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
		for (let i = 0, length = query.snapshotLength; i < length; ++i) {
			console.log(query.snapshotItem(i));
		}
	},
	type: "button",
	value: "XPath Query"
});

const css_row = ewt.row();
const css_input = ewt.el("input", ewt.el("td", css_row), {
	type: "input",
	placeholder: "some > css.selector"
});
ewt.el("input", ewt.el("td", css_row), {
	onclick: function() {
		console.log("CSS Query Results:");
		for (const r of document.querySelectorAll(css_input.value)) {
			console.log(r);
		}
	},
	type: "button",
	value: "CSS Query"
});

let js_func = ewt.el("textarea", ewt.el("td", ewt.row(), { colSpan: 2 }), {
	placeholder: 'function(element) {\n\tconsole.log("Executing on element: ", element);\n}',
	value: 'function(element) {\n\tconsole.log("Executing on element: ", element);\n}',
	rows: 10
});

let exec_row = ewt.row();

ewt.el("input", ewt.el("td", exec_row), {
	onclick: function() {
		const fn = eval(js_func.value);
		const query = document.evaluate(xpath_input.value, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);

		for (let i = 0, length = query.snapshotLength; i < length; ++i) {
			fn(query.snapshotItem(i));
		}
	},
	type: "button",
	value: "Execute over XPath"
});
ewt.el("input", ewt.el("td", exec_row), {
	onclick: function() {
		for (const r of document.querySelectorAll(css_input.value)) {
			fn(r);
		}
	},
	type: "button",
	value: "Execute over CSS"
});
