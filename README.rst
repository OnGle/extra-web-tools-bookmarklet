Extra Web Tools
===============

.. image:: screenshot.png
   :alt: screenshot of extra web tools panel

Just a grab bag of random tools for random annoyances, testing and maybe eventually bug hunting

Build
-----

``yarn run build``
or ``yarn build``
or ``npm run build``

Using
-----

1. Copy the content of ``ewt.build.js`` into the ``URL`` section of a new bookmark
2. Click the bookmark to enable
3. Click ``Close`` to close (note this removes everything from the DOM, to renable click the bookmark again)

Notes
-----

- All output is printed to browser console, so you'll still want devtools open most of the time
- CSS / XPath can pick up elements from EWT itself, I might change that in the future but for now that's just how it is
